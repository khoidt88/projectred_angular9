import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { HomeComponent } from './home/home.component';
import { EditProfileFormComponent } from './profile/edit-profile-form/edit-profile-form.component';
import { GuardService } from './share/service/guard.service';

const routes: Routes = [


  { path: 'home', redirectTo: '' },

  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  {
    path: 'setting',
    component: EditProfileFormComponent,
    canActivate: [GuardService],
  },
  {
    path: 'profile',
    loadChildren: () => import(`./profile/profile.module`).then(m => m.ProfileModule)
  }
  ,

  {
    path: 'article',
    loadChildren: () =>
      import(`./article/article.module`).then((m) => m.ArticleModule),
  },
  { path: '', component: HomeComponent },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
