export interface ArticleRespond {
  articles: Article[];
  articlesCount: number;
}
export interface Article {
  title: string;
  slug: string;
  body: string;
  createdAt: string;
  updatedAt: string;
  tagList: string[];
  description: string;
  author: Author;
  favorited: boolean;
  favoritesCount: number;
}

export interface Author {
  username: string;
  bio?: string;
  image: string;
  following: boolean;
}

export interface Tags {
  tags: string[];
}

export interface articleDeatailRespond {
  article: Article;
}

export interface CommentsRespond {
  comments: Comment[];
}

export interface Comment {
  id: number;
  createdAt: string;
  updatedAt: string;
  body: string;
  author: Author;
}
export interface singleCommentRespond {
  comment: Comment;
}

