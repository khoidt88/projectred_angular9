import {
  articleDeatailRespond,
  Comment,
  CommentsRespond,
  singleCommentRespond,
} from './../Article.model';
import { Component, OnInit } from '@angular/core';
import { Article } from '../Article.model';
import { ArticleService } from '../article.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileService } from 'src/app/profile/profile.service';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.scss'],
})
export class ArticleDetailComponent implements OnInit {
  articleDetail: Article;
  comments: Comment[] = [];
  isMyArticle: boolean;
  username: string;
  isLoggin: boolean = false;
  articleSlug: string;
  constructor(
    private articleService: ArticleService,
    private activetedRoute: ActivatedRoute,
    private route: Router,
    private profileService: ProfileService
  ) {
    this.activetedRoute.params.subscribe((data) => {
      this.articleSlug = data.slug;
      if (localStorage.getItem('token')) {
        this.isLoggin = true;
        this.username = localStorage.getItem('username');
        this.articleService
          .getArticlesDetailsLog(data.slug, localStorage.getItem('token'))
          .subscribe((data: articleDeatailRespond) => {
            this.articleDetail = data.article;
            if (
              data.article.author.username === localStorage.getItem('username')
            ) {
              this.isMyArticle = true;
            } else {
              this.isMyArticle = false;
            }
          });
      } else {
        this.isLoggin = false;
        this.articleService
          .getArticlesDetails(data.slug)
          .subscribe((data: articleDeatailRespond) => {
            this.articleDetail = data.article;
            if (
              data.article.author.username === localStorage.getItem('username')
            ) {
              this.isMyArticle = true;
            } else {
              this.isMyArticle = false;
            }
          });
      }

      this.articleService
        .getCommentsArticle(data.slug)
        .subscribe((data: CommentsRespond) => {
          this.comments = data.comments;
        });
    });
  }
  handleFollow(following: boolean) {
    if (localStorage.getItem('token')) {
      this.articleDetail.author.following = !this.articleDetail.author
        .following;
      if (this.articleDetail.author.following) {
        this.profileService
          .followUser(
            this.articleDetail.author.username,
            localStorage.getItem('token')
          )
          .subscribe((data) => {});
      } else {
        this.profileService
          .unfollowUser(
            this.articleDetail.author.username,
            localStorage.getItem('token')
          )
          .subscribe((data) => {});
      }
    } else {
      alert('You need login to follow');
      this.route.navigate(['login']);
    }
  }
  handleClick(slug: string) {
    if (localStorage.getItem('token')) {
      this.articleDetail.favorited = !this.articleDetail.favorited;
      if (this.articleDetail.favorited) {
        this.articleService
          .favoriteArticle(slug, localStorage.getItem('token'))
          .subscribe((data) => {});
        this.articleDetail.favoritesCount =
          this.articleDetail.favoritesCount + 1;
      } else {
        this.articleService
          .unfavoriteArticle(slug, localStorage.getItem('token'))
          .subscribe((data) => {});
        this.articleDetail.favoritesCount =
          this.articleDetail.favoritesCount - 1;
      }
    } else {
      this.route.navigate(['login']);
    }
  }
  async PostComment(postcontent) {
    await this.articleService
      .addCommentsArticle(
        this.articleSlug,
        postcontent.value,
        localStorage.getItem('token')
      )
      .subscribe((data: singleCommentRespond) => {
        this.comments.unshift(data.comment);
      });
    postcontent.value = '';
  }
  navigate() {
    this.route.navigate(['login']);
  }
  handleDelete(id) {
    this.comments = this.comments.filter((comment) => comment.id !== id);
    this.articleService
      .deleteComments(this.articleSlug, id, localStorage.getItem('token'))
      .subscribe((data) => {});
  }
  ngOnInit(): void {}
  handleEdit(slug: string) {
    this.route.navigate(['', 'article', 'editor', slug]);
  }
}
