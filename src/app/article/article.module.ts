import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ArticleDetailComponent } from './article-detail/article-detail.component';
import { EditArticleFormComponent } from './edit-article-form/edit-article-form.component';
import { CommentsComponent } from './comments/comments.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { MatChipsModule } from '@angular/material/chips';


const routers: Routes = [
  {
    path: 'new-article', component: EditArticleFormComponent
  },
  {
    path: ':slug', component: ArticleDetailComponent
  },
  {
    path: 'editor/:slug_editor', component: EditArticleFormComponent
  }
]


@NgModule({
  declarations: [

    ArticleDetailComponent,
    EditArticleFormComponent,
    CommentsComponent,
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule, FormsModule, MatChipsModule,

    RouterModule.forChild(routers)
  ],
  exports: [

    ArticleDetailComponent,
    EditArticleFormComponent,
  ],
})
export class ArticleModule { }
