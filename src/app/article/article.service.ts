import { HttpClient } from '@angular/common/http';
import { Injectable, Input } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ArticleService {
  articleUrl = 'https://conduit.productionready.io/api/articles';

  slug: string;

  constructor(private http: HttpClient) {}

  getArticles() {
    return this.http.get(this.articleUrl, {
      params: {
        limit: '10',
      },
    });
  }
  getPrivateArticle(token: string) {
    return this.http.get(this.articleUrl, {
      params: {
        limit: '10',
      },
      headers: {
        Authorization: `Token ${token}`,
      },
    });
  }
  getTags() {
    return this.http.get('https://conduit.productionready.io/api/tags');
  }

  paginatorArticles(index: string) {
    return this.http.get(this.articleUrl, {
      params: {
        offset: index,
        limit: '10',
      },
    });
  }

  getArticlesByTag(tagName: string) {
    return this.http.get(this.articleUrl, {
      params: {
        tag: tagName,
        limit: '10',
      },
    });
  }

  getArticlesByAuthor(authorName: string) {
    return this.http.get(this.articleUrl, {
      params: {
        author: authorName,
        limit: '10',
      },
    });
  }

  getArticlesFavoritedByUser(favoritedByUser: string) {
    return this.http.get(this.articleUrl, {
      params: {
        favorited: favoritedByUser,
        limit: '10',
      },
    });
  }

  feedArticles(token: string) {
    return this.http.get(this.articleUrl + '/feed', {
      params: {
        limit: '10',
        offset: '0',
      },
      headers: { Authorization: `Token ${token}` },
    });
  }

  getArticlesDetails(slug: string) {
    return this.http.get(this.articleUrl + '/' + slug);
  }
  getArticlesDetailsLog(slug: string, token: string) {
    return this.http.get(this.articleUrl + '/' + slug, {
      headers: { Authorization: `Token ${token}` },
    });
  }

  createArticle(
    title: string,
    description: string,
    body: string,
    tagList: string[],
    token: string
  ) {
    return this.http.post(
      this.articleUrl,
      {
        article: {
          title: title,
          description: description,
          body: body,
          tagList: tagList,
        },
      },
      { headers: { Authorization: `Token ${token}` } }
    );
  }

  updateArticle(
    slug: string,
    title: string,
    description: string,
    body: string,
    token: string
  ) {
    return this.http.put(
      this.articleUrl + '/' + slug,
      {
        article: {
          title: title,
          description: description,
          body: body,
        },
      },
      { headers: { Authorization: `Token ${token}` } }
    );
  }

  deleteArticle(slug: string) {
    return this.http.delete(this.articleUrl + '/' + slug);
  }

  addCommentsArticle(slug: string, body: string, token: string) {
    return this.http.post(
      `${this.articleUrl}/${slug}/comments`,
      {
        comment: {
          body: body,
        },
      },
      { headers: { Authorization: `Token ${token}` } }
    );
  }

  getCommentsArticle(slug: string) {
    return this.http.get(`${this.articleUrl}/${slug}/comments/`);
  }

  deleteComments(slug: string, id: string, token: string) {
    return this.http.delete(`${this.articleUrl}/${slug}/comments/${id}`, {
      headers: { Authorization: `Token ${token}` },
    });
  }

  favoriteArticle(slug: string, token: string) {
    return this.http.post(
      `${this.articleUrl}/${slug}/favorite`,
      {},
      { headers: { Authorization: `Token ${token}` } }
    );
  }

  unfavoriteArticle(slug: string, token: string) {
    return this.http.delete(`${this.articleUrl}/${slug}/favorite`, {
      headers: { Authorization: `Token ${token}` },
    });
  }

  // get slug
  clickDetail(articleIndex) {
    this.getArticles().subscribe((data) => {
      this.slug = data['articles'][articleIndex].slug;
      console.log(this.slug);
    });
  }
}
