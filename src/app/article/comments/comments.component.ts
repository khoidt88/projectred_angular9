import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Comment } from '../Article.model';


@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
})
export class CommentsComponent implements OnInit {
  @Input() comments: Comment[];
  @Input() user: string;
  @Output() deleteComment = new EventEmitter();
  constructor() {
  }
  handleClick(id: string) {
    this.deleteComment.emit(id);

  }

  ngOnInit(): void { }
}
