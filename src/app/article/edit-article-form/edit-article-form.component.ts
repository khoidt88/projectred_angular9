import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
// import { Tags } from '../Article.model';
import { Article, articleDeatailRespond, Tags } from '../Article.model';
import { ArticleService } from '../article.service';
import { ActivatedRoute, Router } from '@angular/router';
export interface Fruit {
  name: string;
}
@Component({
  selector: 'app-edit-article-form',
  templateUrl: './edit-article-form.component.html',
  styleUrls: ['./edit-article-form.component.scss'],
})
export class EditArticleFormComponent {
  public formUser: FormGroup;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  tags: string[] = [];
  isEdited = false;
  slug: string;

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  newformUser: FormGroup;
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    // Add our fruit
    if ((value || '').trim()) {
      this.tags.push(value);
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }
  remove(tag): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }

  constructor(
    private formBuilder: FormBuilder,
    private articleService: ArticleService,
    private route: Router,
    private activedRoute: ActivatedRoute
  ) {
    this.activedRoute.params.subscribe((data) => {
      if (data.slug_editor !== undefined) {
        this.slug = data.slug_editor;
        this.isEdited = true;
        this.articleService
          .getArticlesDetails(data.slug_editor)
          .subscribe((data: articleDeatailRespond) => {
            console.log(data);
            this.createForm(
              data.article.title,
              data.article.description,
              data.article.body
            );
          });
      } else {
        this.createForm('', '', '');
      }
    });
  }

  createForm(title, about, detail) {
    this.formUser = this.formBuilder.group({
      title: [title, [Validators.required]],
      about: [about, [Validators.required]],
      detail: [detail, [Validators.required]],
      // tag: ['', []],
    });
    // this.formUser.valueChanges.subscribe((data) => {
    //   console.log(data);
    // });
  }

  ngOnInit(): void {}

  onSubmitForm() {
    console.log(this.formUser, this.tags);
    let title = this.formUser.value.title;
    let desc = this.formUser.value.about;
    let detail = this.formUser.value.detail;

    if (this.isEdited == true) {
      this.articleService
        .updateArticle(
          this.slug,
          title,
          desc,
          detail,
          localStorage.getItem('token')
        )
        .subscribe((data: articleDeatailRespond) => {
          this.route.navigate(['', 'article', data.article.slug]);
        });
    } else {
      this.articleService
        .createArticle(
          title,
          desc,
          detail,
          this.tags,
          localStorage.getItem('token')
        )
        .subscribe((data: articleDeatailRespond) => {
          this.formUser = this.formBuilder.group({
            title: ['', [Validators.required]],
            about: ['', [Validators.required]],
            detail: ['', [Validators.required]],
            // tag: ['', []],
          });
          this.route.navigate(['', 'article', data.article.slug]);
        });
    }
  }
}
