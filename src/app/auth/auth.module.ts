import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { ProfileModule } from '../profile/profile.module';
import { MatSnackBarModule } from '@angular/material/snack-bar';
@NgModule({
  declarations: [LoginComponent, SignupComponent],
  imports: [CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule, FormsModule, ProfileModule,
    MatSnackBarModule],
  exports: [
    LoginComponent,
    SignupComponent,

  ],
})
export class AuthModule { }
