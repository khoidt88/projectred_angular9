import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authURL = 'https://conduit.productionready.io/api/users';
  username: string;
  isChangeUsername: Subject<string> = new Subject<string>();

  constructor(private http: HttpClient) {
    this.isChangeUsername.subscribe((newUsername) => {
      this.username = newUsername;
    })
  }

  changeUsername(newUsername) {
    this.isChangeUsername.next(newUsername);
  }

  login(email: string, password: string) {
    return this.http.post(this.authURL + '/login',
      {
        "user": {
          "email": email,
          "password": password
        }
      })
  }
  isLogin(): boolean {
    return !!localStorage.getItem('token');
  }
  logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('username');

  }
  signup(username: string, email: string, password: string) {
    return this.http.post(this.authURL, {
      "user": {
        "username": username,
        "email": email,
        "password": password
      }
    })
  }
}
