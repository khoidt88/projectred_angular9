import { HttpErrorResponse } from '@angular/common/http';
import { Component, } from '@angular/core';
import { NgForm, } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UserRespond } from 'src/app/profile/profile.model';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  constructor(private authService: AuthService, private router: Router, private _snackBar: MatSnackBar) { }
  handleSubmit(form: NgForm) {
    this.authService.login(form.value.email, form.value.password).subscribe((data: UserRespond) => {

      this._snackBar.open("Login Successful", '', { duration: 2000 })
      localStorage.setItem('token', data.user.token);
      localStorage.setItem('username', data.user.username);
      this.authService.changeUsername(data.user.username);
      this.router.navigate([''])
    }, (err: HttpErrorResponse) => {
      this._snackBar.open("Email and Password invalid , Please try again", '', { duration: 2000 })
    })

  }
}
