import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { Router } from '@angular/router';
import { Article, articleDeatailRespond, ArticleRespond, Tags } from '../article/Article.model';

import { ArticleService } from '../article/article.service';
export interface TabDisplay {
  label: string,
  data: Article[],
  count: number
}
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  article: Article[] = [];
  tags: string[] = [];
  offset = 0;
  feed: Article[] = [];
  pageSize: number;
  tabs: TabDisplay[] = [];
  constructor(private articleService: ArticleService, private route: Router) {

  }
  async ngOnInit() {

    if (localStorage.getItem('token')) {
      this.tabs = [];
      const token: string = localStorage.getItem('token');
      this.articleService.feedArticles(token).subscribe((datafeed: ArticleRespond) => {
        this.tabs.push({ label: 'Feed', data: datafeed.articles, count: datafeed.articlesCount });
        this.pageSize = datafeed.articlesCount
        this.articleService.getPrivateArticle(token).subscribe((data: ArticleRespond) => {
          this.tabs.push({ label: 'Global', data: data.articles, count: data.articlesCount });
        })
      })
    } else {
      await this.articleService.getArticles().subscribe((data: ArticleRespond) => {
        this.tabs.push({ label: 'Global', data: data.articles, count: data.articlesCount })
        this.pageSize = data.articlesCount
      });
    }

    this.articleService.getTags().subscribe(((data: Tags) => {
      this.tags = data.tags
    }))

  }
  focusChange(event: MatTabChangeEvent) {

    let tabFiltered = this.tabs.filter((item: TabDisplay) => item.label === event.tab.textLabel)
    this.pageSize = tabFiltered[0].count

  }
  handleLike(slug: string) {
    if (localStorage.getItem('token')) {
      const token: string = localStorage.getItem('token');
      this.articleService.favoriteArticle(slug, token).subscribe((data: articleDeatailRespond) => {
      })


    } else {
      this.route.navigate(['login'])
    }
  }
  handleUnlike(slug: string) {
    if (localStorage.getItem('token')) {
      const token: string = localStorage.getItem('token');
      this.articleService.unfavoriteArticle(slug, token).subscribe((data: articleDeatailRespond) => {
      })
    } else {
      this.route.navigate(['login'])
    }
  }
  hanldeTag(tag: string) {
    this.articleService.getArticlesByTag(tag).subscribe((data: ArticleRespond) => {
      if (this.tabs.length > 2) {
        this.tabs.pop();
      }
      this.tabs.push({
        label: tag, data: data.articles, count: data.articlesCount
      })
    })

  }
  async handlePage(event: PageEvent) {

    if (event.pageIndex < event.previousPageIndex) {
      this.offset = this.offset - 10
    } else {
      this.offset = this.offset + 10
    }
    await this.articleService.paginatorArticles((this.offset) + '')
      .subscribe((data: ArticleRespond) => {
        this.article = data.articles;
      })
  }
}
