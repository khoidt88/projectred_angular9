import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth/auth.service';
import { GuardService } from '../share/service/guard.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {
  displayUser: string;

  constructor(private guardService: GuardService, private authService: AuthService) {
    // if (this.authService.isLogin()) {
    //   this.displayUser = localStorage.getItem('username');
    // }
    this.authService.isChangeUsername.subscribe((newUsername) => {
      this.displayUser = newUsername;
    })
    this.authService.changeUsername(localStorage.getItem('username'));
  }
}
