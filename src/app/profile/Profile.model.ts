export interface UserRespond {
  user: User;
}

export interface User {
  id: number;
  email: string;
  createdAt: string;
  updatedAt: string;
  username: string;
  bio?: any;
  image?: any;
  token: string;
}

export interface ProfileRespond {
  profile: Profile;
}

export interface Profile {
  username: string;
  bio?: any;
  image: string;
  following: boolean;
}
