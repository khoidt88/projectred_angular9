export interface User {
    username: string;
    bio: string;
    image: string;
    following: boolean;
    email: string;
    password: string;

}