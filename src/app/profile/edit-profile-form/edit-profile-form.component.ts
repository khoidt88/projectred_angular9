import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UserRespond } from '../profile.model';

import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-edit-profile-form',
  templateUrl: './edit-profile-form.component.html',
  styleUrls: ['./edit-profile-form.component.scss']
})
export class EditProfileFormComponent implements OnInit {

  username: string;
  bio: any;
  image: string;
  following: boolean;
  email: string;

  editProfile: FormGroup;
  submitted = false;


  constructor(private profileService: ProfileService, private formBuilder: FormBuilder) {


  }

  ngOnInit(): void {

    let token = localStorage.getItem('token');

    this.editProfile = this.formBuilder.group({
      image: '',
      username: ['', Validators.required],
      bio: '',
      email: ['', Validators.required],
      password: ['', [Validators.minLength(6)]]

    });


    this.profileService.getCurrentUser(token).subscribe((data: UserRespond) => {

      //console.log(data);

      this.editProfile.patchValue({
        image: data['user'].image,
        bio: data['user'].bio,
        username: data['user'].username,
        email: data['user'].email
      });
    })
  }
  get f() { return this.editProfile.controls; }

  onSubmit() {

    this.submitted = true;
    if (this.editProfile.invalid) {
      return;
    }

    this.profileService.updateUser(this.editProfile.value['user'], this.editProfile.value['email'], this.editProfile.value['bio'], this.editProfile.value('image'));
  }
}
