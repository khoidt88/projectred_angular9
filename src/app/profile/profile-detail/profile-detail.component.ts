import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Article, ArticleRespond } from 'src/app/article/Article.model';

import { ArticleService } from 'src/app/article/article.service';
import { Profile, ProfileRespond } from '../profile.model';


import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-profile-detail',
  templateUrl: './profile-detail.component.html',
  styleUrls: ['./profile-detail.component.scss'],
})
export class ProfileDetailComponent implements OnInit {
  myArticles: Article[] = [];
  myFavoriteArticles: Article[] = [];
  profile: Profile;
  constructor(
    private profileService: ProfileService,
    private articleService: ArticleService,
    private activedRoute: ActivatedRoute
  ) {
    this.activedRoute.params.subscribe(data => {
      this.articleService.getArticlesByAuthor(data.username).subscribe((data: ArticleRespond) => {
        this.myArticles = data.articles
      })
      this.articleService.getArticlesFavoritedByUser(data.username).subscribe(
        (data: ArticleRespond) => { this.myFavoriteArticles = data.articles }
      )
      this.profileService.getProfile(data.username).subscribe((data: ProfileRespond) => {
        this.profile = data.profile
      })
    })
  }

  ngOnInit(): void {

  }
}
