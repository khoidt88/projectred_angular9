import { Component, Input, OnInit } from '@angular/core';
import { Profile } from '../Profile.model';
import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-profile-user',
  templateUrl: './profile-user.component.html',
  styleUrls: ['./profile-user.component.scss']
})
export class ProfileUserComponent implements OnInit {

  username: 'Test1223';
  bio: string;
  image: string;
  following: boolean;

  constructor(private profileService: ProfileService) {

  }

  ngOnInit(): void {

    this.profileService.getProfile(this.username).subscribe((data) => {
      console.log(data); // day no khong in ra gi ơ  console ok anh 
      this.username = data['profile'].username;
      this.bio = data['profile'].bio;
      this.image = data['profile'].image;
      this.following = data['following'].following;


    })
  }

}
