import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileDetailComponent } from './profile-detail/profile-detail.component';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { EditProfileFormComponent } from './edit-profile-form/edit-profile-form.component';
import { MatInputModule } from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { ShareModule } from '../share/share.module';

const routes: Routes = [
  { path: ':username', component: ProfileDetailComponent }
]
@NgModule({
  declarations: [ProfileDetailComponent, EditProfileFormComponent],
  imports: [CommonModule, MatButtonModule, ReactiveFormsModule, MatInputModule, MatFormFieldModule, MatTabsModule, MatIconModule, RouterModule.forChild(routes), ShareModule],

  exports: [ProfileDetailComponent, EditProfileFormComponent]
})
export class ProfileModule { }
