import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {


  profileUrl = 'https://conduit.productionready.io/api';

  constructor(private http: HttpClient) { }

  getProfile(username: string) {
    return this.http.get(this.profileUrl + '/profiles/' + username)
  }

  followUser(username: string, token: string) {
    return this.http.post(this.profileUrl + '/profiles/' + username + '/follow/', {}, { headers: { Authorization: `Token ${token}` } })
  }

  unfollowUser(username: string, token: string) {
    return this.http.delete(this.profileUrl + '/profiles/' + username + '/follow', { headers: { Authorization: `Token ${token}` } });
  }

  getCurrentUser(token: string) {
    return this.http.get(this.profileUrl + '/user', { headers: { Authorization: `Token ${token}` } })
  }

  updateUser(user: string, email: string, bio: any, image: string) {
    return this.http.put(this.profileUrl + '/' + user, {
      user: {
        email: email,
        bio: bio,
        image: image,
      },
    });
  }
  logOut() {
    localStorage.clear();
  }

}
