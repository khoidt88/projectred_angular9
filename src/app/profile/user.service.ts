import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../profile/profile.module';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  profileUrl = 'https://conduit.productionready.io/api/profiles';
  followUserURL = 'https://conduit.productionready.io/api/profiles/:username/follow';
  unfollowURL = 'https://conduit.productionready.io//api/profiles/:username/follow';

  constructor(private http: HttpClient) { }

  getProfile(username: string) {
    return this.http.get(`${this.profileUrl}/:${username}`);
  }

}
