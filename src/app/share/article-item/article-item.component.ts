import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Article } from 'src/app/article/Article.model';



@Component({
  selector: 'app-article-item',
  templateUrl: './article-item.component.html',
  styleUrls: ['./article-item.component.scss'],
})
export class ArticleItemComponent implements OnInit {
  @Input() articleItem: Article;
  @Input() articleIndex: number;
  @Output() likeArticle = new EventEmitter();
  @Output() unlikeArticle = new EventEmitter();

  constructor(private route: Router) { }

  ngOnInit(): void { }
  handleClick(slug: string) {
    this.articleItem.favorited = !this.articleItem.favorited
    if (this.articleItem.favorited) {
      this.likeArticle.emit(slug)
      this.articleItem.favoritesCount = this.articleItem.favoritesCount + 1
    } else {
      this.unlikeArticle.emit(slug)
      this.articleItem.favoritesCount = this.articleItem.favoritesCount - 1
    }


  }

  showDetail(slug: string) {

    this.route.navigate(['', 'article', slug])
  }
}
