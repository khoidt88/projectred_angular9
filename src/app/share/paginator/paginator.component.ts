import { EventEmitter, Input } from '@angular/core';
import { Output } from '@angular/core';
import { Component } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';


@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent {
  @Input() articleLength: Number;
  @Output() pageChange = new EventEmitter()
  constructor() { }
  handleChange(event: PageEvent) {
    this.pageChange.emit(event);
  }
}
