import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ArticleService {
  articleUrl = 'https://conduit.productionready.io/api/articles';

  constructor(private http: HttpClient) {}

  getArticles() {
    return this.http.get(this.articleUrl, {
      params: {
        limit: '10',
      },
    });
  }

  paginatorArticles() {
    return this.http.get(this.articleUrl, {
      params: {
        offset: '0',
        limit: '10'
      }
    });
  }

  getArticlesByTag(tagName: string) {
    return this.http.get(this.articleUrl, {
      params: {
        tag: tagName,
        limit: '10',
      },
    });
  }

  getArticlesByAuthor(authorName: string) {
    return this.http.get(this.articleUrl, {
      params: {
        author: authorName,
        limit: '10',
      },
    });
  }

  getArticlesFavoritedByUser(favoritedByUser: string) {
    return this.http.get(this.articleUrl, {
      params: {
        favorited: favoritedByUser,
        limit: '10',
      },
    });
  }

  feedArticles() {
    return this.http.get(this.articleUrl, {
      params: {},
    });
  }

  getArticlesDetails(slug: string) {
    return this.http.get(this.articleUrl + slug)
  }


}
