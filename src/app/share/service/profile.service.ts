import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


interface Profile {
  articles: Article[];
  articlesCount: number;
}

interface Article {
  title: string;
  slug: string;
  body: string;
  createdAt: string;
  updatedAt: string;
  tagList: string[];
  description: string;
  author: Author;
  favorited: boolean;
  favoritesCount: number;
}

interface Author {
  username: string;
  bio?: any;
  image: string;
  following: boolean;
}
@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  profileUrl = 'https://conduit.productionready.io/api';

  constructor(private http: HttpClient) { }

  get(username: string){
    return this.http.get(this.profileUrl + '/profiles/' + username)
  }
  favorite(username: string){
    return this.http.get(this.profileUrl + '/profile/' +username +'/favorites/')
  }
}
