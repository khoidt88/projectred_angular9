import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TagsComponent } from './tags/tags.component';
import { MatCardModule } from '@angular/material/card';
import { PaginatorComponent } from './paginator/paginator.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatChipsModule } from '@angular/material/chips';
import { ArticleItemComponent } from './article-item/article-item.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [TagsComponent, PaginatorComponent, ArticleItemComponent],
  imports: [CommonModule, MatCardModule, MatPaginatorModule, MatChipsModule, MatIconModule, MatButtonModule],
  exports: [TagsComponent, PaginatorComponent, ArticleItemComponent],
})
export class ShareModule { }
