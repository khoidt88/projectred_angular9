import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';


@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent implements OnInit {

  @Input() tagsInput: string[];
  @Output() tagSelected = new EventEmitter();


  constructor() { }

  ngOnInit(): void {
  }
  handleClick(tag: string) {
    this.tagSelected.emit(tag);
  }

}
